defmodule Bowling do
  def score([[a,b]|t]) when a + b == 10 do
    [c|d] = hd(t)
    a + b + c + score(t)
  end
  def score(game) do
    game
    |> List.flatten
    |> Enum.filter(&is_number/1)
    |> List.foldl(0, &+/2)
  end
end
